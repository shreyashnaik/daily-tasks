package testday2;

import day2.SequentialSearch;
import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestSequentialSearch {
    private SequentialSearch sequentialSearch;
    private int input[];
    private int empty[];
    @BeforeEach
    public void setup()
    {
        sequentialSearch = new SequentialSearch();
         input = new int[]{-5,52,14,23,20,7,-9,78};
         empty = new int[]{};
    }
    @Test
    public void testNull()
    {
        Assert.assertFalse(sequentialSearch.sequentialSeach(null,10));
        Assert.assertFalse(sequentialSearch.sequentialSeach(empty,10));

    }

    @Test
    public void testSuccessSearch()
    {
        Assert.assertTrue(sequentialSearch.sequentialSeach(input,-9));
    }

}
