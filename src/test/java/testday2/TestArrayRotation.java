package testday2;

import day2.ArrayRotation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestArrayRotation {
    private ArrayRotation arrayRotation;
    @BeforeEach
    public void setup()
    {
        arrayRotation = new ArrayRotation();
    }
    @Test
    public void testSuccess()
    {
        int[] input = {1,2,3,4};
        int k = 2;
        int [] result = arrayRotation.rotate(input,k);
        Assertions.assertEquals(1,result[k]);
    }
    @Test
    public void testNullOrEmpty()
    {
        int input[] = {};
        Assertions.assertEquals(null,arrayRotation.rotate(input,4) );
        Assertions.assertEquals(null,arrayRotation.rotate(null,4) );

    }
}
