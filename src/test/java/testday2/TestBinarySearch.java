package testday2;

import day2.BinarySearch;
import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestBinarySearch {
    private BinarySearch binarySearch;
    private int[]input;
    private int []empty;

     @BeforeEach
    public void setup()
     {
         binarySearch = new BinarySearch();
         input = new int[]{12,15,4,7,8,-85,12,54};
         empty = new int[]{};
     }
     @Test
    public void testEmpty()
     {
         Assert.assertFalse(binarySearch.binarySearch(null,10));
     }
     @Test
    public void testSuccessSearch()
     {
         Assert.assertTrue(binarySearch.binarySearch(input,12));
     }
     @Test
    public void testFailureSearch()
     {
         Assert.assertFalse(binarySearch.binarySearch(input,52));
     }
}

