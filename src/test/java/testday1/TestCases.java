package testday1;

import day1.SumArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCases {
    private SumArray object;
    @BeforeEach
    public void setup()
    {
         object = new SumArray();
    }

    @Test
    public void testNull()
    {


        Assertions.assertEquals(0,object.sum(null));
    }
    @Test
    public void testNotNull(){
        int [] input = {1,2,3,4,5};
        Assertions.assertEquals(15,object.sum(input));
    }

}
