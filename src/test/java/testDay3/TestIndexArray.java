package testDay3;

import day3.IndexArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestIndexArray {
    private IndexArray indexArray;
    @BeforeEach
    public void setup()
    {
        indexArray = new IndexArray();
    }
    @Test
    public void testSuccess()
    {
        int input[]={8, -1, 6, 1, 9, 3, 2, 7, 4, -1};
        int output[]= indexArray.indexArray(input);
        Assertions.assertEquals(-1,output[0]);
    }
}
