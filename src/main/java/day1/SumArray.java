package day1;

public class SumArray {
    public int sum(int [] elements)
    {
        if(elements == null || elements.length == 0)
        {
            return 0;
        }
        int sum = 0;
        for(int i= 0 ;i<elements.length;i++)
        {
           sum+=elements[i];
        }
        return sum;
    }
}
