package day2;

public class ArrayRotation {
    public int[] rotate(int[]input,int k)
    {
        if(input==null || input.length == 0)
        {
            return null;
        }
        int temp;
        for(int i=0;i<input.length/2;i++)
        {
            temp = input[i];
            input[i] = input[(i+k)%(input.length)];
            input[(i+k)%(input.length)] = temp;
        }

        return input;
    }
}
