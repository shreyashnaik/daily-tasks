package day2;

import java.util.Arrays;

public class BinarySearch {
    public boolean binarySearch(int []input, int target)
    {
        boolean result = false;
        if(input == null || input.length==0)
        {
            return result;
        }else{
            Arrays.sort(input);
            return search(0,input.length-1,input,target);
        }

    }
    public boolean search(int start,int end,int [] input,int target)
    {
        int mid = (start+end)/2;
        if(start>end)
        {
            return false;
        }
        if(target == input[mid])
        {
            return true;
        }else if(target<input[mid])
        {
            return search(start,mid-1,input,target);
        }
        else
        {
            return search(mid+1,end,input,target);
        }

    }
}
