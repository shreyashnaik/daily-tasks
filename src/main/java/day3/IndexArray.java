package day3;

import java.util.HashSet;
import java.util.Set;

public class IndexArray {
    public int[] indexArray(int[] input)
    {
        Set<Integer> set = new HashSet<>();
        for(int i=0;i<input.length;i++)
        {
            set.add(input[i]);
        }
        int [] output = new int[input.length];
        for(int i=0;i<output.length;i++)
        {
            if(set.contains(i))
            {
                output[i] = i;
            }else
            {
                output[i]=-1;
            }
        }
        for(int i :output)
        {
            System.out.println(i);
        }
        return output;
    }
}
